const mongoose = require('mongoose');
const mongoDB = 'mongodb://127.0.0.1:27017/interview';
mongoose.connect(mongoDB ,{ useNewUrlParser: true, createIndexes: true,useUnifiedTopology: true , useFindAndModify: false});
mongoose.Promise = global.Promise;

module.exports = {
    User: require('../users/user.model'),
    UserLog: require('../user_log/user_log.model')
};