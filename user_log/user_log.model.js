const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { ObjectId } = Schema.Types;

const schema = new Schema({
    loginTime: {
        type: Date,
        default: Date.now
    },
    logoutTime: {
        type: Date
    },
    userId: {
        type: ObjectId,
        ref: 'User',
        index: true
    },
    clientIp: {
        type: String
    }

});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('userLog', schema);