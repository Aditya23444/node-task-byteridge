﻿const express = require('express');
const router = express.Router();
const userLogService = require('./userlog.service');
const jwt_decode = require('jwt-decode');



// routes
router.get('/audit', getAll);
router.put('/update', update);


module.exports = router;

function getAll(req, res, next) {
    const token = req.headers.authorization.replace(/^Bearer\s/, '');
    const decoded = jwt_decode(token);
    if(decoded.role !== 'AUDITOR'){
        res.status(401).json({ message: 'Invalid role' })
    }
    userLogService.getAll(req.query.skip)
        .then(userlog => res.json(userlog))
        .catch(err => next(err));
}

function update(req, res, next) {
    const token = req.headers.authorization.replace(/^Bearer\s/, '');
    const decoded = jwt_decode(token);
    userLogService.update(decoded.sub)
        .then(() => res.json({}))
        .catch(err => next(err));
}



