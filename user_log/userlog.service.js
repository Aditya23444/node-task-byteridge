﻿
const db = require('_helpers/db');
const userLog = db.UserLog;


module.exports = {
    getAll,
    update
};

async function getAll(skip) {
    const skipval = skip? skip: 0

    return await userLog.find()
        .select('-hash')
        .populate("userId")
        .limit(10)
        .skip(Number(skipval))
        .sort({
            name: 'asc'
        })
}

async function update(userId){
    return  userLog.updateOne({userId: userId}, {logoutTime: new Date()}, {upsert: true})
}
